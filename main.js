const express = require('express');
const addRateDB = require('./controller/addRate');
const getQuoteShipment = require('./controller/getQuote');
const createShipmentDB = require('./controller/createShipment');
const getShipmentDB = require('./controller/getShipment');
const deleteShipmentDB = require('./controller/deleteShipment');
const listShipmentDB = require('./controller/listShipment')
const app = express();
const bodyParser = require('body-parser');

//mongoose
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/shipment', { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);
// get reference to database
const connection = mongoose.connection;

connection.on('error', console.error.bind(console, 'connection error:'));

connection.once('open', () => {
    console.log('Mongo DB connections');
});

const jsonParser = bodyParser.json();
const base_url = "http://localhost:8081";

cors = require('./cors');
app.use(cors());
//addRate() to DB
app.post('/rate/add-many',jsonParser, (req, res) => {addRateDB.addRateMany(req, res)});

//getQuote()
app.post('/api/shipment/getquote', jsonParser, (req, res) => {getQuoteShipment.getQuote(req, res)});

//createShipment()
app.post('/api/shipment/createshipment', jsonParser, (req, res) => {createShipmentDB.createShipment(req, res)});

//getShipment()
app.get('/api/shipment/getShipment/:ref', (req, res) => {getShipmentDB.getShipment(req, res)});

//listShipment()
app.get('/api/shipment/getShipmentList', (req, res) => {listShipmentDB.listShipment(req, res)});

//deleteShipment()
app.delete('/api/shipment/delete/:ref', (req, res) => {deleteShipmentDB.deleteShipment(req, res)});

const server = app.listen(8081, () => {
    const host = server.address().address;
    const port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});