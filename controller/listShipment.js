const express = require('express');
const bodyParser = require('body-parser')
const md5 = require("blueimp-md5");
const fs = require("fs");

const ShipmentModel = require('../model/shipment');
const RateModel = require('../model/rate');
const { responseData } = require('./supportFunc');

const jsonParser = bodyParser.json();



exports.listShipment = (req, res) => {
    if(req.query.ref){
        ShipmentModel.findOne({ref: req.query.ref}, [], {skip: 0, limit: 10, sort: {created_at: 1}}, (err, data) => {
            var mesenger = "List Successful"
            if (err) var mesenger = "Error";
            res.header("Access-Control-Allow-Origin", "*");
            res.json(responseData(data, mesenger, ""));
        });
    } else {
        ShipmentModel.find({}, [], {skip: 0, limit: 10, sort: {created_at: 1}}, (err, data) => {
            var mesenger = "List Successful"
            if (err) var mesenger = "Error";
            res.header("Access-Control-Allow-Origin", "*");
            res.json(responseData(data, mesenger, ""));
        });
    }
};