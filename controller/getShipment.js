const express = require('express');
const bodyParser = require('body-parser')
const md5 = require("blueimp-md5");
const fs = require("fs");

const ShipmentModel = require('../model/shipment');
const RateModel = require('../model/rate');

const jsonParser = bodyParser.json();


function responseData(data = {}, mesenger = "Successful", code = 200) {
    return {
        "success": code == 200 ? true : false,
        "message": mesenger,
        "status": code,
        "data": data ? data : {},
    };
}; 


exports.getShipment = (req, res) => {
    const ref = req.params.ref;
    ShipmentModel.findOne({ref: ref}, (err, data) => {
        
            const mesenger = "Successful"
            if (err) mesenger = "Error";
            res.header("Access-Control-Allow-Origin", "*");
            res.json(responseData(data, mesenger, ""));
        }
    );
};