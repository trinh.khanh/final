const express = require('express');
const bodyParser = require('body-parser')
const md5 = require("blueimp-md5");
const fs = require("fs");

const ShipmentModel = require('../model/shipment');
const RateModel = require('../model/rate');
const { responseDataShip, getWeight, makeRef } = require('./supportFunc');

const jsonParser = bodyParser.json();


exports.createShipment = (req, res) => {
    try {
        const shipmentBody = req.body.data;

        const weight = getWeight(shipmentBody.package.grossWeight.amount);

        RateModel.findOne({ weight: { $lte: weight } }, ['_id', 'price'], {
            skip: 0,
            limit: 1,
            sort: { weight: -1 }
        }, (err, data) => {
            if (err) {
                res.header("Access-Control-Allow-Origin", "*");
                res.json(responseDataShip({}));
            } else {
                var data = data ? data : {};

                shipmentBody['cost'] = data['price'];
                shipmentBody['ref'] = makeRef(10);

                ShipmentModel.create(shipmentBody, (error, dataCreate) => {

                    var dataAdd = dataCreate ? dataCreate : {};

                    responseCreate = {
                        "ref": dataAdd.ref,
                        "create_at": dataAdd.create_at,
                        "cost": dataAdd.cost,
                    };
                    res.header("Access-Control-Allow-Origin", "*");
                    res.json(responseDataShip(responseCreate));
                });
            }
        }
        );
    } catch (e) {
        res.header("Access-Control-Allow-Origin", "*");
        res.json(responseDataShip({}));
    }
};