const express = require('express');
const bodyParser = require('body-parser')
const md5 = require("blueimp-md5");
const fs = require("fs");
const {responseDataShip , getWeight } = require('./supportFunc');

const ShipmentModel = require('../model/shipment');
const RateModel = require('../model/rate');

const jsonParser = bodyParser.json();


exports.getQuote = (req, res) => {
    const shipmentBody = req.body.data;
    const weight = getWeight(shipmentBody.package.grossWeight.amount);
    RateModel.findOne({weight: {$lte: weight}}, ['_id', 'price'], {
            skip: 0,
            limit: 1,
            sort: {weight: -1}
        }, (err, data) => {
            var data = data ? data : {};
            var dataQuote = [{
                "id": data['_id'],
                "amount": data['price'],
            }];
            res.header("Access-Control-Allow-Origin", "*");
            res.json(responseDataShip(dataQuote));
        }
    );
};