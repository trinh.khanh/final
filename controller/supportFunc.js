const responseDataShip = (data = {}) => {
    return {
        "data": data ? data : {},
    };
};

const getWeight = (weight) => {
    Number(weight);
    if (weight < 1) {
        weight = null;
    }
    else if (weight < 250) {
        weight = 250;
    }
    else if (weight > 15000) {
        weight = 15000;
    };
    return weight;
}

const makeRef = (length = 10) => {
    let result = '';
    let characters = '0975094094khancascannnnacasannhtx';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
} 

function responseData(data = {}, mesenger = "Successful", code = 200) {
    return {
        "success": code == 200 ? true : false,
        "message": mesenger,
        "status": code,
        "data": data ? data : {},
    };
}; 

module.exports = {
    responseDataShip,
    getWeight,
    makeRef,
    responseData
}