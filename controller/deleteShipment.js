const express = require('express');
const bodyParser = require('body-parser')
const md5 = require("blueimp-md5");
const fs = require("fs");

const ShipmentModel = require('../model/shipment');
const RateModel = require('../model/rate');
const { responseData } = require('./supportFunc');

const jsonParser = bodyParser.json();


exports.deleteShipment = (req, res) => {
    const ref = req.params.ref;
    ShipmentModel.findOneAndRemove({ref: ref}, (err, data) => {
            var mesenger = "Your Shipment has been deleted."
            var code = 200;
            if (!data || err) {
                code = 500;
                mesenger = "Shipment has not found";
            }
            res.header("Access-Control-Allow-Origin", "*");
            res.json(responseData(data, mesenger, code));
        }
    );
};