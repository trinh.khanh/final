const express = require('express');
const bodyParser = require('body-parser')
const md5 = require("blueimp-md5");
const fs = require("fs");

const { responseDataShip } = require('./supportFunc')
const ShipmentModel = require('../model/shipment');
const RateModel = require('../model/rate');

const jsonParser = bodyParser.json();


// function responseDataShip(data = {}) {
//     return {
//         "data": data ? data : {},
//     };
// };

exports.addRateMany = (req, res) => {
    var rates = fs.readFileSync( __dirname + "/" + "../rates.json", "utf8");
    rates = JSON.parse(rates);
    console.log(rates);
    RateModel.insertMany(rates, function (err, data) {
        res.header("Access-Control-Allow-Origin", "*");
        res.json(responseDataShip(data));
    });
};
